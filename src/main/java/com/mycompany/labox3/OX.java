/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.labox3;

/**
 *
 * @author informatics
 */
class OX {

    static boolean checkWin(Character[][] table, Character currentPlayer) {
        if (checkRow(table, currentPlayer, 0)) {
            return true;
        } else {
            if (checkRow(table, currentPlayer, 1)) {
                return true;
            } else {
                if (checkRow(table, currentPlayer, 2)) {
                    return true;
                }
            }
        }

        if (checkCol(table, currentPlayer, 0)) {
            return true;
        } else {
            if (checkCol(table, currentPlayer, 1)) {
                return true;
            } else {
                if (checkCol(table, currentPlayer, 2)) {
                    return true;
                }
            }
        }

        if (checkTopLeft_to_BottomRight(table, currentPlayer)) {
            return true;
        }

        if (checkTopRight_to_BottomLeft(table, currentPlayer)) {
            return true;
        }

        return false;

    }

    static boolean checkDraw(Character[][] table) {
        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table[row].length; col++) {
                if (table[row][col] == '-') {
                    return false;
                }
            }
        }
        return !checkWin(table, 'X') && !checkWin(table, 'O');
    }

    private static boolean checkRow(Character[][] table, Character currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }

    private static boolean checkCol(Character[][] table, Character currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);
    }

    private static boolean checkTopLeft_to_BottomRight(Character[][] table, Character currentPlayer) {
        return table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer);
    }

    private static boolean checkTopRight_to_BottomLeft(Character[][] table, Character currentPlayer) {
        return table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][0].equals(currentPlayer);
    }

}
