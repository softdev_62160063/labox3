/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.labox3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class CalculationUnitTest {

    public CalculationUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow1_O_output_true() {
        Character[][] table = {{'O', 'O', 'O'}, {'-', '-', '-'}, {'-', '-', '-'}};
        Character currentPlayer = 'O';
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow2_O_output_true() {
        Character[][] table = {{'-', '-', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}};
        Character currentPlayer = 'O';
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow3_O_output_true() {
        Character[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'O', 'O', 'O'}};
        Character currentPlayer = 'O';
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol1_X_output_true() {
        Character[][] table = {{'X', '-', '-'}, {'X', '-', '-'}, {'X', '-', '-'}};
        Character currentPlayer = 'X';
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol2_X_output_true() {
        Character[][] table = {{'-', 'X', '-'}, {'-', 'X', '-'}, {'-', 'X', '-'}};
        Character currentPlayer = 'X';
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol3_X_output_true() {
        Character[][] table = {{'-', '-', 'X'}, {'-', '-', 'X'}, {'-', '-', 'X'}};
        Character currentPlayer = 'X';
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinTopLeft_to_BottomRight_O_output_true() {
        Character[][] table = {{'O', '-', '-'}, {'-', 'O', '-'}, {'-', '-', 'O'}};
        Character currentPlayer = 'O';
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinTopRight_to_BottomLeft_X_output_true() {
        Character[][] table = {{'-', '-', 'X'}, {'-', 'X', '-'}, {'X', '-', '-'}};
        Character currentPlayer = 'X';
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckDraw1() {
        Character[][] table = {{'X', 'O', 'X'}, {'O', 'X', 'O'}, {'O', 'X', 'O'}};
        boolean result = OX.checkDraw(table);
        assertEquals(true, result);
    }

    @Test
    public void testCheckDraw2() {
        Character[][] table = {{'X', 'O', 'X'}, {'X', 'O', 'O'}, {'O', 'X', 'X'}};
        boolean result = OX.checkDraw(table);
        assertEquals(true, result);
    }

    @Test
    public void testCheckDraw3() {
        Character[][] table = {{'O', 'X', 'O'}, {'O', 'X', 'X'}, {'X', 'O', 'X'}};
        boolean result = OX.checkDraw(table);
        assertEquals(true, result);
    }

    @Test
    public void testCheckDraw4() {
        Character[][] table = {{'X', 'O', 'X'}, {'X', 'O', 'X'}, {'O', 'X', 'O'}};
        boolean result = OX.checkDraw(table);
        assertEquals(true, result);
    }

    @Test
    public void testCheckDraw5() {
        Character[][] table = {{'O', 'X', 'O'}, {'O', 'X', 'O'}, {'X', 'O', 'X'}};
        boolean result = OX.checkDraw(table);
        assertEquals(true, result);
    }

}
